import { IRouteComponentProps } from 'umi';
import Head from '@/components/head/index';
import Nav from '@/components/nav';
import './index.less';

export default function index({
  children,
  location,
  route,
  history,
  match,
}: IRouteComponentProps) {
  if (location.pathname === '/') {
    history.replace('/dashboard'); 
  }
  return (
    <div className="layout">
      <div className="navBox">
        <Nav routes={route.routes}></Nav>
      </div>
      <div className="layoutMain">
        <div className="head">
          <Head></Head>
        </div>
        <div className="content">{children}</div>
      </div>
    </div>
  );
}
