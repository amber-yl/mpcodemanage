import { useCallback, useState } from 'react';
import { useHistory, request } from 'umi';
import { setToken } from '@/utils/comm';
import { message, Checkbox } from 'antd';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import styles from './index.less';

export default function Login() {
  const history = useHistory();

  const [user, setUser] = useState({
    email: '',
    password: '',
  });

  const login = async () => {
    if (user.email === '') {
      message.error('账号不能为空！');
    } else if (user.password === '') {
      message.error('密码不能为空！');
    } else {
      let res = await request('http://localhost:3000/auth/login', {
        method: 'post',
        data: {
          ...user,
        },
      });
      if (res.code === 0) {
        message.error(res.msg);
      } else if (res.code === 1) {
        setToken(res.data);
        message.success(res.msg);
        history.replace('/dashboard');
      }
    }
  };

  const onChange = (e: CheckboxChangeEvent) => {
    console.log(`checked = ${e.target.checked}`);
  };

  const goRegister = () => {
    history.push('/register');
  };

  return (
    <div className={styles.login}>
      <div className="main">
        <div className="left">
          <div className="top">
            <div className="log">QK</div>
            <div className="title">乾坤项目管理系统</div>
          </div>
          <div className="message">寄蜉蝣于天地，渺沧海之一粟。</div>
          <div className="image">
            <img src="http://localhost:3000/images/login.png" alt="" />
          </div>
        </div>
        <div className="right">
          <div className="loginform">
            <div className="title">欢迎登录</div>
            <div className="email">
              <div className="name">
                <span>Email</span>
              </div>
              <div className="input">
                <input
                  type="email"
                  name=""
                  id=""
                  value={user.email}
                  onChange={(e) => {
                    setUser({ ...user, email: e.target.value });
                  }}
                  placeholder="qiankun@project.com"
                />
              </div>
            </div>
            <div className="password">
              <div className="name">
                <span>密码</span>
              </div>
              <div className="input">
                <input
                  type="password"
                  name=""
                  id=""
                  value={user.password}
                  onChange={(e) => {
                    setUser({ ...user, password: e.target.value });
                  }}
                  placeholder="· · · · · · · · ·"
                />
              </div>
            </div>
            <div className="isme">
              <div className="me-left">
                <Checkbox onChange={onChange}>
                  <span className="text">记住我</span>
                </Checkbox>
              </div>
              <div className="me-right">
                <span>忘记密码？</span>
              </div>
            </div>
            <div className="login" onClick={login}>
              立即登录
            </div>
            <div className="goregister">
              还没有账号？<span onClick={goRegister}>去注册</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
