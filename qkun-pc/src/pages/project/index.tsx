import React, { FC } from 'react';
import { IndexModelState, ConnectProps, Loading, connect } from 'umi';

import style from '../styles/project.module.less';
import LeftBox from './components/leftBox';
import ProjectContent from './components/content';

interface PageProps extends ConnectProps {
  program: IndexModelState;
  loading: boolean;
}

const project: FC<PageProps> = ({ program, dispatch }) => {
  const { name } = program;

  let data = [
    {
      id: 'PN0000001',
      title: '医疗应用(IOS 原生)',
    },
    {
      id: 'PN0000002',
      title: '送餐服务系统',
    },
    {
      id: 'PN0000003',
      title: '财富企业网站',
    },
    {
      id: 'PN0000004',
      title: '规划期应用',
    },
    {
      id: 'PN0000005',
      title: '时间追踪器',
    },
    {
      id: 'PN0000006',
      title: '内部项目系统',
    },
  ];

  return (
    <div className={style.projectBox}>
      <header>
        <div className={style.projectTitle}>项目</div>
        <div className={style.addProject}>添加项目</div>
      </header>
      <main>
        <div className={style.projectMainLeftBox}>
          <LeftBox data={data}></LeftBox>
        </div>
        <div className={style.projectMainCenterBox}>
          <ProjectContent />
        </div>
      </main>
    </div>
  );
};

export default connect(
  ({ program, loading }: { program: IndexModelState; loading: Loading }) => ({
    program,
    loading: loading.models.program,
  }),
)(project);
