import React, { useState } from 'react';

import style from '../../styles/project.module.less';

export default function TypeOne() {
  let [data, setData] = useState({
    arr: [
      {
        name: '按钮动画',
        startTime: '',
        endTime: '',
        group: '', // 组
        user: {
          // 执行人
          name: 'xxx',
          img: '',
          email: 'xxxx',
          phone: 'xxxx',
        },
        priority: 1, // 优先级：0 1 2
        status: 0, // 状态：0:待开始 1:现在做 2:进行中 3:验收中 4:已完成
        describe: '',
      },
      {
        name: '预加载动画',
        startTime: '',
        endTime: '',
        group: '',
        user: {
          name: 'xxx',
          img: '',
          email: 'xxxx',
          phone: 'xxxx',
        },
        priority: 0,
        status: 0,
        describe: '',
      },
      {
        name: '用户调研',
        startTime: '',
        endTime: '',
        group: '',
        user: {
          name: 'xxx',
          img: '',
          email: 'xxxx',
          phone: 'xxxx',
        },
        priority: 1,
        status: 4,
        describe: '',
      },
      {
        name: '思维导图',
        startTime: '',
        endTime: '',
        group: '',
        user: {
          name: 'xxx',
          img: '',
          email: 'xxxx',
          phone: 'xxxx',
        },
        priority: 1,
        status: 2,
        describe: '',
      },
      {
        name: '用户体验草图',
        startTime: '',
        endTime: '',
        group: '',
        user: {
          name: 'xxx',
          img: '',
          email: 'xxxx',
          phone: 'xxxx',
        },
        priority: 2,
        status: 4,
        describe: '',
      },
      {
        name: '用户体验登录+注册',
        startTime: '',
        endTime: '',
        group: '',
        user: {
          name: 'xxx',
          img: '',
          email: 'xxxx',
          phone: 'xxxx',
        },
        priority: 1,
        status: 2,
        describe: '',
      },
      {
        name: 'UI登录+注册',
        startTime: '',
        endTime: '',
        group: '',
        user: {
          name: 'xxx',
          img: '',
          email: 'xxxx',
          phone: 'xxxx',
        },
        priority: 1,
        status: 3,
        describe: '',
      },
      {
        name: '其他界面UI',
        startTime: '',
        endTime: '',
        group: '',
        user: {
          name: 'xxx',
          img: '',
          email: 'xxxx',
          phone: 'xxxx',
        },
        priority: 0,
        status: 1,
        describe: '',
      },
    ],
    arrIndex: 0,
  });

  function statusToStr(num: number) {
    // 状态数字变字符串
    //0:待开始 1:现在做 2:进行中 3:验收中 4:已完成
    if (num === 0) return '待开始';
    else if (num === 1) return '现在做';
    else if (num === 2) return '进行中';
    else if (num === 3) return '验收中';
    else if (num === 4) return '已完成';
  }

  return (
    <div className={style.ProjectTypeOne}>
      {/* 正在进行的任务 标题 */}
      {data.arr.filter((item) => item.status !== 0).length > 0 ? (
        <div className="ProjectTypeOneTitle">当前任务</div>
      ) : (
        ''
      )}
      {/*  正在进行的任务  */}
      {data.arr
        .filter((item) => item.status !== 0)
        .map((item, index) => (
          // 正在进行的任务项
          <div className="projectTypeOneItem" key={index}>
            <div className="projectTypeOneItemName">
              <div className="projectTypeOneItemTitle">任务名称</div>
              <div className="projectTypeOneItemText">{item.name}</div>
            </div>
            <div className="projectTypeOneItemStart">
              <div className="projectTypeOneItemTitle">预计时间</div>
              <div className="projectTypeOneItemText">2天4小时</div>
            </div>
            <div className="projectTypeOneItemEnd">
              <div className="projectTypeOneItemTitle">实际时间</div>
              <div className="projectTypeOneItemText">1天2小时</div>
            </div>
            <div className="projectTypeOneItemPerson">
              <div className="projectTypeOneItemTitle">执行人</div>
              <div className="projectTypeOneItemText"></div>
            </div>
            <div className="projectTypeOneItemPriority">
              <div className="projectTypeOneItemTitle">优先等级</div>
              <div
                className={`projectTypeOneItemText ${
                  item.priority === 0
                    ? 'green'
                    : item.priority === 1
                    ? 'yellow'
                    : 'red'
                }`}
              >
                {item.priority === 0
                  ? '↓低'
                  : item.priority === 1
                  ? '↑中'
                  : '↑高'}
              </div>
            </div>
            <div className="projectTypeOneItemStatus">
              {item.status !== 0 ? (
                <>
                  <div className="projectTypeOneItemTab">
                    {statusToStr(item.status)}
                  </div>
                </>
              ) : (
                ''
              )}
            </div>
            <div className="projectTypeOneItemPlanBox">
              <div className="projectTypeOneItemPlan"></div>
            </div>
          </div>
        ))}
      {data.arr.filter((item) => item.status === 0).length > 0 ? (
        <div className="ProjectTypeOneTitle">待开始任务</div>
      ) : (
        ''
      )}
      {data.arr
        .filter((item) => item.status === 0)
        .map((item, index) => (
          // 正在进行的任务项
          <div className="projectTypeOneItem" key={index}>
            <div className="projectTypeOneItemName">
              <div className="projectTypeOneItemTitle">任务名称</div>
              <div className="projectTypeOneItemText">{item.name}</div>
            </div>
            <div className="projectTypeOneItemStart">
              <div className="projectTypeOneItemTitle">预计时间</div>
              <div className="projectTypeOneItemText">2天4小时</div>
            </div>
            <div className="projectTypeOneItemEnd">
              <div className="projectTypeOneItemTitle">实际时间</div>
              <div className="projectTypeOneItemText">0小时</div>
            </div>
            <div className="projectTypeOneItemPerson">
              <div className="projectTypeOneItemTitle">执行人</div>
              <div className="projectTypeOneItemText"></div>
            </div>
            <div className="projectTypeOneItemPriority">
              <div className="projectTypeOneItemTitle">优先等级</div>
              <div
                className={`projectTypeOneItemText ${
                  item.priority === 0
                    ? 'green'
                    : item.priority === 1
                    ? 'yellow'
                    : 'red'
                }`}
              >
                {item.priority === 0
                  ? '↓低'
                  : item.priority === 1
                  ? '↑中'
                  : '高'}
              </div>
            </div>
            <div className="projectTypeOneItemStatus">
              {item.status !== 0 ? (
                <>
                  <div className="projectTypeOneItemTab">
                    {statusToStr(item.status)}
                  </div>
                </>
              ) : (
                ''
              )}
            </div>
            <div className="projectTypeOneItemPlanBox">
              <div className="projectTypeOneItemPlan"></div>
            </div>
          </div>
        ))}
    </div>
  );
}
