import React, { ReactNode, useState } from 'react';
import { Button, Modal, Form, Input, Select, DatePicker } from 'antd';
import 'dayjs/locale/zh-cn';
import locale from 'antd/es/date-picker/locale/zh_CN';
import './addModal.less'
type LayoutType = Parameters<typeof Form>[0]['layout'];
const { TextArea } = Input;
const AddModal: React.FC = () => {
    const [loading, setLoading] = useState(false);
    //显示弹框
    const [open, setOpen] = useState(false);

    const handleOk = () => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
            setOpen(false);
        }, 3000);
    };

    const handleCancel = () => {
        setOpen(false);
    };
    const [form] = Form.useForm();
    return (
        <>
            <Modal
                open={open}
                title="添加项目"
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="submit" type="primary" loading={loading} onClick={handleOk} style={{ borderRadius: '6px' }}>
                        保存任务
                    </Button>,
                ]}
            >
                <Form
                    form={form}
                    layout="vertical"
                >
                    <Form.Item label="任务名称" >
                        <Input placeholder="登录+注册UI设计" style={{ borderRadius: '4px' }} />
                    </Form.Item>
                    <Form.Item label="任务分组">
                        <Select
                            defaultValue="UI/UX设计师"
                            options={[
                                { value: '程序员', label: '程序员' },
                                { value: 'UI/UX设计师', label: 'UI/UX设计师' },
                            ]}
                            style={{ borderRadius: '4px' }}
                        />
                    </Form.Item>
                    <Form.Item label="优先等级">
                        <Select
                            defaultValue="中"
                            options={[
                                { value: '低', label: '低' },
                                { value: '中', label: '中' },
                                { value: '高', label: '高' },
                            ]}
                            style={{ borderRadius: '4px' }}
                        />
                    </Form.Item>
                    <Form.Item style={{ marginBottom: 0, marginLeft: 0 }}>
                        <Form.Item
                            label="开始日期"
                            style={{ display: 'inline-block', width: '40%' }}
                        >
                            <DatePicker locale={locale} />
                        </Form.Item>
                        <Form.Item label="结束日期" style={{ display: 'inline-block', width: '40%' }}>
                            <DatePicker locale={locale} />
                        </Form.Item>
                    </Form.Item>
                    <Form.Item label="执行人">
                        <Select
                            defaultValue="Neptune"
                            options={[
                                { value: 'Mercury', label: 'Mercury' },
                                { value: 'Venus', label: 'Venus' },
                                { value: 'Earth', label: 'Earth' },
                                { value: 'Mars', label: 'Mars' },
                                { value: 'Jupiter', label: 'Jupiter' },
                                { value: 'Saturn', label: 'Saturn' },
                                { value: 'Uranus', label: 'Uranus' },
                                { value: 'Neptune', label: 'Neptune' },
                            ]}
                            style={{ borderRadius: '4px' }}
                        />
                    </Form.Item>
                    <Form.Item label="描述">
                        <TextArea rows={4} placeholder="maxLength is 30" maxLength={30} />
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
};

export default AddModal;