import React from 'react';

import style from '../../styles/project.module.less';
export default function leftBox(props: any) {
  console.log(props);
  let { data } = props;
  return (
    <>
      <div className={style.projectMainLeftTitle}>
        当前项目 <span>^</span>
      </div>
      <div className={style.projectMainLeftContent}>
        {data.map((item: any) => (
          <div className={style.projectItem} key={item.id}>
            <div className={style.projectId}>{item.id}</div>
            <div className={style.projectTitle}>{item.title}</div>
          </div>
        ))}
      </div>
    </>
  );
}
