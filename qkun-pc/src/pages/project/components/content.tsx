import React, { useState } from 'react';

import style from '../../styles/project.module.less';
import TypeOne from './typeOne';
import TypeTwo from './typeTwo';
import TypeThree from './typeThree';

export default function ProjectContent(props: any) {
  let [showType, setShowType] = useState({
    arr: [
      {
        text: '文',
        component: TypeOne,
      },
      {
        text: 'tab',
        component: TypeTwo,
      },
      {
        text: '甘',
        component: TypeThree,
      },
    ],
    defaultIndex: 0,
    component: TypeOne,
  });
  return (
    <>
      <header>
        <div className={style.ProjectContentTitle}>任务列表</div>
        <div className={style.ProjectContentCenter}>
          {showType.arr.map((item, index) => (
            <div
              className={showType.defaultIndex === index ? style.active : ''}
              onClick={() => {
                setShowType({
                  ...showType,
                  defaultIndex: index,
                  component: item.component,
                });
              }}
              key={index}
            >
              {item.text}
            </div>
          ))}
        </div>
        <div className={style.ProjectContentCheck}>
          <div>筛</div>
        </div>
      </header>
      <main className={style.projectContentMain}>{<showType.component />}</main>
    </>
  );
}
