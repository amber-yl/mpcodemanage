import React, { useState, useRef } from 'react';
import { request, useHistory } from 'umi';
import { Select, Input, Radio, message } from 'antd';
import type { RadioChangeEvent } from 'antd';
import styles from './index.less';

export default function Register() {
  const history = useHistory();

  // 手机号 - input
  const [tel, setTel] = useState('');
  // 短信验证码 - input
  const [telCode, setTelCode] = useState({
    c1: '',
    c2: '',
    c3: '',
    c4: '',
  });
  // 判断是否返回验证码 是否时间过期
  const [isCode, setIsCode] = useState(false);
  // 60倒计时
  let [times, setTimes] = useState(60);
  // 邮箱 - input
  const [email, setEmail] = useState('');
  // 密码 - input
  const [password, setPassword] = useState('');

  // 注册步骤组件切换高亮
  const [activeIndex, setActiveIndex] = useState(0);
  // content2组件单选框切换
  const [value, setValue] = useState(1);
  // content3组件团队人数列表
  const [teamNumber, setTeamNumber] = useState([
    { text: '只有我' },
    { text: '2-5' },
    { text: '6-10' },
    { text: '11-20' },
    { text: '21-40' },
    { text: '41-50' },
    { text: '51-100' },
    { text: '101-500' },
  ]);
  // content3组件团队人数切换高亮
  const [teamActive, setTeamActive] = useState(0);
  // content1组件实例
  const content1: React.ForwardedRef<HTMLDivElement> = useRef(null);
  // content2组件实例
  const content2: React.ForwardedRef<HTMLDivElement> = useRef(null);
  // content3组件实例
  const content3: React.ForwardedRef<HTMLDivElement> = useRef(null);
  // content4组件实例
  const content4: React.ForwardedRef<HTMLDivElement> = useRef(null);
  // alert弹框控制
  const [isShow, setIsShow] = useState(false);

  // 获取验证码
  const getTelCode = async () => {
    if (tel === '') {
      message.error('手机号不能为空！');
    } else if (!/^1[3-9]\d{9}$/.test(tel)) {
      message.error('请输入正确格式的手机号哦！');
    } else {
      let res = await request('http://localhost:3000/register/getTelCode', {
        method: 'post',
        data: {
          tel,
        },
      });
      if (res.code === 0) {
        message.error(res.msg);
      } else {
        let { tel, code, time } = res.data;
        setTelCode({
          ...telCode,
          c1: code.slice(0, 1),
          c2: code.slice(1, 2),
          c3: code.slice(2, 3),
          c4: code.slice(3),
        });
        setIsCode(true);
        testTime();
      }
    }
  };

  // 60秒倒计时函数
  const testTime = () => {
    let aa = setInterval(function () {
      setTimes(times--);
      if (times == 0) {
        setTimes(60);
        setIsCode(false);
        clearInterval(aa);
      }
    }, 1000);
  };

  const onChange = (e: RadioChangeEvent) => {
    console.log('radio checked', e.target.value);
    setValue(e.target.value);
  };

  // content3组件团队人数function
  const tabTeamNumber = (index: number) => {
    setTeamActive(index);
  };

  // content1 to content2
  const content1to2 = () => {
    if (content1.current && content2.current) {
      content1.current.style.display = 'none';
      content2.current.style.display = 'flex';
    }
    setActiveIndex(1);
  };
  // content2 to content3
  const content2to3 = () => {
    if (content2.current && content3.current) {
      content2.current.style.display = 'none';
      content3.current.style.display = 'flex';
    }
    setActiveIndex(2);
  };
  // content3 to content4
  const content3to4 = () => {
    if (content3.current && content4.current) {
      content3.current.style.display = 'none';
      content4.current.style.display = 'flex';
    }
    setActiveIndex(3);
  };
  // content4 to alert
  const content4ToAlert = () => {
    setIsShow(true);
  };

  // content4 to content3
  const content4to3 = () => {
    if (content4.current && content3.current) {
      content4.current.style.display = 'none';
      content3.current.style.display = 'flex';
    }
    setActiveIndex(2);
  };
  // content3 to content2
  const content3to2 = () => {
    if (content3.current && content2.current) {
      content3.current.style.display = 'none';
      content2.current.style.display = 'flex';
    }
    setActiveIndex(1);
  };
  // content2 to content1
  const content2to1 = () => {
    if (content2.current && content1.current) {
      content2.current.style.display = 'none';
      content1.current.style.display = 'flex';
    }
    setActiveIndex(0);
  };

  // 去首页
  const goDashboard = () => {
    history.replace('/dashboard');
  };

  return (
    <div className={styles.register}>
      <div className="main">
        {/* 注册页左侧 */}
        <div className="left">
          <div className="top">
            <div className="log">QK</div>
          </div>
          <div className="start">开始注册</div>
          <div className="flow">
            <div
              className={`list ${
                activeIndex === 0 ||
                activeIndex === 1 ||
                activeIndex === 2 ||
                activeIndex === 3
                  ? 'active'
                  : null
              } ${
                activeIndex === 1 || activeIndex === 2 || activeIndex === 3
                  ? 'bctive'
                  : null
              }`}
            >
              <div className="item">
                <span className="circle">✓</span>
                <span className="text">验证您的手机</span>
              </div>
              <div className="item">
                <span className="line"></span>
              </div>
            </div>
            <div
              className={`list ${
                activeIndex === 1 || activeIndex === 2 || activeIndex === 3
                  ? 'active'
                  : null
              } ${activeIndex === 2 || activeIndex === 3 ? 'bctive' : null}`}
            >
              <div className="item">
                <span className="circle">✓</span>
                <span className="text">自我介绍</span>
              </div>
              <div className="item">
                <span className="line"></span>
              </div>
            </div>
            <div
              className={`list ${
                activeIndex === 2 || activeIndex === 3 ? 'active' : null
              } ${activeIndex === 3 ? 'bctive' : null}`}
            >
              <div className="item">
                <span className="circle">✓</span>
                <span className="text">公司概况</span>
              </div>
              <div className="item">
                <span className="line"></span>
              </div>
            </div>
            <div className={`list ${activeIndex === 3 ? 'active' : null}`}>
              <div className="item">
                <span className="circle">✓</span>
                <span className="text">邀请团队成员</span>
              </div>
            </div>
          </div>
        </div>
        <div className="right">
          {/* 注册第一步 */}
          <div className="content1" ref={content1}>
            <main>
              <div className="form">
                <div className="title">
                  <div className="page">步骤 {activeIndex + 1}/4</div>
                  <div className="tit">验证您的手机</div>
                </div>
                <div className="tel">
                  <div className="name">手机号码</div>
                  <div className="inp">
                    <div className="inp-left">
                      <select name="" id="">
                        <option value="+86">+86</option>
                        <option value="+41">+41</option>
                        <option value="+36">+36</option>
                      </select>
                    </div>
                    <div className="inp-right">
                      <input
                        type="tel"
                        name=""
                        id="tel"
                        placeholder="请输入您的手机号"
                        value={tel}
                        onChange={(e) => setTel(e.target.value)}
                      />
                      {!isCode ? (
                        <span onClick={getTelCode} className="span_f">
                          获取验证码
                        </span>
                      ) : (
                        <span className="span_t">{times}S后重新获取</span>
                      )}
                    </div>
                  </div>
                </div>
                <div className="code">
                  <div className="name">短信验证码</div>
                  <div className="number">
                    <input
                      type="text"
                      name=""
                      id="c1"
                      value={telCode.c1}
                      onChange={(e) =>
                        setTelCode({ ...telCode, c1: e.target.value })
                      }
                    />
                    <input
                      type="text"
                      name=""
                      id="c2"
                      value={telCode.c2}
                      onChange={(e) =>
                        setTelCode({ ...telCode, c2: e.target.value })
                      }
                    />
                    <input
                      type="text"
                      name=""
                      id="c3"
                      value={telCode.c3}
                      onChange={(e) =>
                        setTelCode({ ...telCode, c3: e.target.value })
                      }
                    />
                    <input
                      type="text"
                      name=""
                      id="c4"
                      value={telCode.c4}
                      onChange={(e) =>
                        setTelCode({ ...telCode, c4: e.target.value })
                      }
                    />
                  </div>
                </div>
                {isCode ? (
                  <div className="msg">
                    <span className="icon">i</span>
                    <span className="time">
                      短信验证码已发送至您的手机号码 {tel},有效期 1分钟
                    </span>
                  </div>
                ) : null}
                <div className="email">
                  <div className="name">Email</div>
                  <input
                    type="email"
                    name=""
                    id="email"
                    placeholder="qiankun@project.com"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div className="password">
                  <div className="name">输入密码</div>
                  <input
                    type="password"
                    name=""
                    id="password"
                    placeholder="· · · · · · · · ·"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
              </div>
            </main>
            <footer>
              <div className="btn" onClick={content1to2}>
                <span>下一步</span>
              </div>
            </footer>
          </div>
          {/* 注册第二步 */}
          <div className="content2" ref={content2}>
            <main>
              <div className="form">
                <div className="title">
                  <div className="page">步骤 {activeIndex + 1}/4</div>
                  <div className="tit">自我介绍</div>
                </div>
                <div className="whyUse">
                  <div className="name">您为什么要使用该服务？</div>
                  <div className="select">
                    <select name="" id="">
                      <option value="工作">工作</option>
                      <option value="学习">学习</option>
                      <option value="生活">生活</option>
                    </select>
                  </div>
                </div>
                <div className="describe">
                  <div className="name">什么最能描述你？</div>
                  <div className="select">
                    <select name="" id="">
                      <option value="工作">公司高管</option>
                      <option value="学习">UI设计师</option>
                      <option value="生活">前端工程师</option>
                      <option value="生活">后端架构师</option>
                    </select>
                  </div>
                </div>
                <div className="describe2">
                  <div className="name">什么最能描述你？</div>
                  <div className="radio">
                    <Radio.Group onChange={onChange} value={value}>
                      <Radio value={1}>是的</Radio>
                      <Radio value={2}>不</Radio>
                    </Radio.Group>
                  </div>
                </div>
              </div>
            </main>
            <footer>
              <div className="goback" onClick={content2to1}>
                <span className="zuo">⇇</span>
                <span>上一页</span>
              </div>
              <div className="btn" onClick={content2to3}>
                <span>下一步</span>
              </div>
            </footer>
          </div>
          {/* 注册第三步 */}
          <div className="content3" ref={content3}>
            <main>
              <div className="form">
                <div className="title">
                  <div className="page">步骤 {activeIndex + 1}/4</div>
                  <div className="tit">公司概况</div>
                </div>
                <div className="corporation">
                  <div className="name">贵公司名称</div>
                  <input type="text" name="" id="" placeholder="公司名称" />
                </div>
                <div className="business">
                  <div className="name">业务方向</div>
                  <div className="select">
                    <select name="" id="">
                      <option value="工作">信息技术与编程</option>
                      <option value="学习">项目管理与维护</option>
                      <option value="生活">前端研发与创新</option>
                      <option value="生活">后端架构与深入</option>
                    </select>
                  </div>
                </div>
                <div className="team-number">
                  <div className="name">你的团队有多少人？</div>
                  <div className="list">
                    {teamNumber.map((item, index) => {
                      return (
                        <div
                          className={`item ${
                            teamActive === index ? 'active' : ''
                          }`}
                          key={index}
                          onClick={() => tabTeamNumber(index)}
                        >
                          {item.text}
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </main>
            <footer>
              <div className="goback" onClick={content3to2}>
                <span className="zuo">⇇</span>
                <span>上一页</span>
              </div>
              <div className="btn" onClick={content3to4}>
                <span>下一步</span>
              </div>
            </footer>
          </div>
          {/* 注册第四页 */}
          <div className="content4" ref={content4}>
            <main>
              <div className="form">
                <div className="title">
                  <div className="page">步骤 {activeIndex + 1}/4</div>
                  <div className="tit">邀请团队成员</div>
                </div>
                <div className="email">
                  <div className="name">成员邮箱</div>
                  <input
                    type="text"
                    name=""
                    id=""
                    placeholder="qiankun@project.com"
                  />
                </div>
                <div className="addmember">
                  <span className="icon">十</span>
                  <span>添加另一个成员</span>
                </div>
              </div>
            </main>
            <footer>
              <div className="goback" onClick={content4to3}>
                <span className="zuo">⇇</span>
                <span>上一页</span>
              </div>
              <div className="btn" onClick={content4ToAlert}>
                <span>下一步</span>
              </div>
            </footer>
          </div>
        </div>
      </div>
      {isShow ? (
        <div className="alert">
          <div className="img">
            <img src="http://localhost:3000/images/login.png" alt="" />
          </div>
          <div className="text">恭喜您，注册成功！</div>
          <div className="btn" onClick={goDashboard}>
            现在开始吧
          </div>
        </div>
      ) : null}
    </div>
  );
}
