import React from 'react';
import type { BadgeProps } from 'antd';
import { Badge, Calendar, ConfigProvider } from 'antd';
import type { Moment } from 'moment';
import styles from './index.less';

import moment from 'moment';
import 'moment/locale/zh-cn';
import locale from 'antd/lib/locale/zh_CN';

const getListData = (value: Moment) => {
  let listData;
  switch (value.date()) {
    case 8:
      listData = [
        { type: 'warning', content: 'This is warning event.' },
        { type: 'success', content: 'This is usual event.' },
      ];
      break;
    case 10:
      listData = [
        { type: 'warning', content: 'This is warning event.' },
        { type: 'success', content: 'This is usual event.' },
        { type: 'error', content: 'This is error event.' },
      ];
      break;
    case 15:
      listData = [
        { type: 'warning', content: 'This is warning event' },
        { type: 'success', content: 'This is very long usual event。。....' },
        { type: 'error', content: 'This is error event 1.' },
        { type: 'error', content: 'This is error event 2.' },
        { type: 'error', content: 'This is error event 3.' },
        { type: 'error', content: 'This is error event 4.' },
      ];
      break;
    default:
  }
  return listData || [];
};

const getMonthData = (value: Moment) => {
  if (value.month() === 8) {
    return 1394;
  }
};

export default function CalendarComponent() {
  const monthCellRender = (value: Moment) => {
    const num = getMonthData(value);
    return num ? (
      <div className="notes-month">
        <section>{num}</section>
        <span>Backlog number</span>
      </div>
    ) : null;
  };

  const dateCellRender = (value: Moment) => {
    const listData = getListData(value);
    return (
      <ul className="events">
        {listData.map((item) => (
          <li key={item.content}>
            <Badge
              status={item.type as BadgeProps['status']}
              text={item.content}
            />
          </li>
        ))}
      </ul>
    );
  };

  return (
    <div className={styles.calendar}>
      <div className="title">
        <div className="text">日历</div>
        <div className="button">
          <span className="icon">十</span>
          <span className="add">添加事件</span>
        </div>
      </div>
      <div className="main">
        <ConfigProvider locale={locale}>
          <Calendar
            defaultValue={moment(
              `${new Date().toLocaleDateString()}`,
              'YYYY-MM-DD',
            )}
            // 用 dateCellRender 和 monthCellRender 函数来自定义需要渲染的数据。
            dateCellRender={dateCellRender}
            monthCellRender={monthCellRender}
          />
        </ConfigProvider>
      </div>
    </div>
  );
}
