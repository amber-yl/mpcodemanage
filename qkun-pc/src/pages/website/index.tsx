import React from 'react';
import '@/styles/website.less';
import { Link } from 'umi';

export default function website() {
  const log = () => {
    console.log('22222');
  };
  return (
    <div className="website">
      <div className="top">
        <div className="title">网站项目</div>
        <button className="add">添加文件夹</button>
      </div>
      <div className="con">
        <div className="left">
          <div className="left-lef">
            <h4>您的项目数据</h4>
            <div className="tit">
              添加项目数据、创建专题页面、编辑数据、与团队成员共享信息
            </div>
          </div>
          <img src="http://localhost:3000/img/door/picture.png" alt="" />
        </div>
        <div className="right">
          <div className="tit">当前项目</div>
          <div className="h3">
            10 <img src="http://localhost:3000/img/door/save.png" alt="" />
          </div>
          <div className="green">成长+3</div>
          <div className="pro">上个月正在进行的项目</div>
        </div>
      </div>
      <div className="bottom">
        <div
          className="item"
          onClick={() => {
            log();
          }}
        >
          <img src="http://localhost:3000/img/door/文件夹紫色.png" alt="" />
          <Link to={{ pathname: '/website/medical' }}>医疗应用</Link>
          <div className="tit">医疗应用</div>
          <div className="page">5页</div>
        </div>
        <div className="item">
          <img src="http://localhost:3000/img/door/文件夹紫色.png" alt="" />
          <div className="tit">财富网站</div>
          <div className="page">5页</div>
        </div>
      </div>
    </div>
  );
}
