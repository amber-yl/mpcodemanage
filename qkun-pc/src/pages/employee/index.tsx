import React, { useState, useEffect } from 'react';
import '../../styles/employee.less';
import { Button } from 'antd';
import { PlusOutlined, FilterOutlined } from '@ant-design/icons';
import { request } from 'umi';
import MyModal from '@/components/Modal';
const Employee: React.FC = () => {
  const [isActive, setIsActive] = useState(0);
  const [typeList, setTypeList] = useState([])
  //头部tab切换列表
  const emList = ['列表', '状态'];
  //状态数据模拟

  //modal对话框相关操作
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const showModal = () => {
    setOpen(true);
  };

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    setOpen(false);
  };
  //获取数据
  useEffect(() => {
    request('http://localhost:3000/employee/get', {
      method: "GET"
    }).then(res => {
      console.log(res);
      setTypeList(res)
    })
  }, [])
  return (
    <>
      <MyModal
        title={'添加员工'}
        cancelText={'取消'}
        okText={'确定添加'}
        open={open}
        handleOk={handleOk}
        confirmLoading={confirmLoading}
        handleCancel={handleCancel}
      />
      <div className="emTop">
        <div className="employee">
          <span className="text">员工</span>
          <span className="num">（28）</span>
        </div>
        <div className="emTab">
          {emList.map((i, d) => {
            return (
              <div
                key={d}
                className={isActive === d ? 'emListActive' : ''}
                onClick={() => {
                  setIsActive(d);
                }}
              >
                {i}
              </div>
            );
          })}
        </div>
        <div className="emIcon">
          <div className="emSearch">
            <FilterOutlined style={{ fontSize: '1.24rem' }} />
          </div>
          <Button type="primary" icon={<PlusOutlined />} onClick={showModal}>
            添加员工
          </Button>
        </div>
      </div>
      <div className='emBox'>
        {isActive === 0 ? (
          <div>
            {
              typeList.map((i: any) => {
                return (<div className="emItem" key={i._id}>
                  <div className="emImg">
                    <img src="http://localhost:3000/employeeImages/img.png" alt="" />
                  </div>
                  <div className="emName">
                    <div className="top">{i.typeName}</div>
                    <div className="bottom">{i.typeEmail}</div>
                  </div>
                  <div className="emSex">
                    <div className="top">性别</div>
                    <div className="bottom">{i.sex}</div>
                  </div>
                  <div className="emDate">
                    <div className="top">日期</div>
                    <div className="bottom">{i.typeDate}</div>
                  </div>
                  <div className="emAge">
                    <div className="top">年龄</div>
                    <div className="bottom">{i.age}</div>
                  </div>
                  <div className="emPosition">
                    <div className="top">职位</div>
                    <div className="bottom">{i.typePosition}</div>
                  </div>
                  <div className="emGrade">{i.typeGrade}</div>
                  <div className="emDian"></div>
                </div>)
              })
            }
          </div>
        ) : (
          <div className="typeBox">
            {typeList.map((i: any) => {
              return (
                <div className="typeItem" key={i._id}>
                  <div className={i.sex === "男" ? 'top' : 'top1'}>
                    <div className="vator">
                      <img
                        src="http://localhost:3000/employeeImages/img.png"
                        alt=""
                      />
                    </div>
                    <div className="name">{i.typeName}</div>
                    <div className="position">{i.typePosition}</div>
                    <div className="grade">{i.typeGrade}</div>
                  </div>
                  <div className="typeBottom">
                    <div className="typeItem1">
                      <div className="typeTop">{i.beginNum}</div>
                      <div className="typeBottom">待开始任务</div>
                    </div>
                    <div className="typeItem1">
                      <div className="typeTop">{i.conductNum}</div>
                      <div className="typeBottom">进行中任务</div>
                    </div>
                    <div className="typeItem1">
                      <div className="typeTop">{i.checkNum}</div>
                      <div className="typeBottom">已审查任务</div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        )}
      </div>
    </>
  );
};

export default Employee;
