import React from 'react';
import style from './index.less';
import { useHistory } from 'umi';
export default function NearList(props: any) {
  let history = useHistory();
  let nearList = [
    {
      myColor: 'new',
      title: '新部门介绍，标题文字介绍显示 ',
      imgSrc: 'http://localhost:3000/tx/top.png',
      time: '今天| 下午5:00',
      data: '2h',
    },
    {
      myColor: 'birthday',
      title: 'Mars的生日',
      imgSrc: 'http://localhost:3000/tx/down.png',
      time: '今天|上午:10:00',
      data: '5h',
    },
    {
      myColor: 'new',
      title: '新部门介绍，标题文字介绍显示 ',
      imgSrc: 'http://localhost:3000/tx/top.png',
      time: '今天| 下午5:00',
      data: '2h',
    },
    {
      myColor: 'birthday',
      title: 'Mercury的生日',
      imgSrc: 'http://localhost:3000/tx/down.png',
      time: '今天|上午:10:00',
      data: '5h',
    },
  ];
  return (
    <div className={style.nearList}>
      <p
        className={style.goBack}
        onClick={() => {
          history.push('/dashboard');
        }}
      >
        {' '}
        <img
          src="http://localhost:3000/tx/arrowleft.png"
          alt=""
        /> 返回仪表盘{' '}
      </p>
      <div className={style.head}>
        <p> 最近事件 </p>
        <button> 添加事件 </button>
      </div>
      <div className={style.list}>
        {nearList.map((item, index) => {
          return (
            <div className={style.item} key={index}>
              <div className={style.task}>
                <div className={style.content}>
                  <span>
                    {' '}
                    <img
                      src="http://localhost:3000/tx/company.png"
                      alt=""
                    />{' '}
                    {item.title}{' '}
                  </span>
                  <img src={item.imgSrc} alt="" />
                </div>
                <div className={style.data}>
                  <span className={style.date}>{item.time} </span>
                  <span> {item.data} </span>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
