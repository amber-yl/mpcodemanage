import React from 'react';
import styles from './index.less';
import { useState } from 'react';
import { useHistory } from 'umi';
export default function dashboard(props: any) {
  let [user, setUser] = useState('某某某');
  let history = useHistory();
  let arrList = [
    {
      imgSrc: 'http://localhost:3000/tx/tx.png',
      name: '张伟成',
      postName: '用户界面/交互设计师',
      grade: '中级',
    },
    {
      imgSrc: 'http://localhost:3000/tx/tx.png',
      name: '张伟成',
      postName: '用户界面/交互设计师',
      grade: '中级',
    },
    {
      imgSrc: 'http://localhost:3000/tx/tx.png',
      name: '张伟成',
      postName: '用户界面/交互设计师',
      grade: '中级',
    },
    {
      imgSrc: 'http://localhost:3000/tx/tx.png',
      name: '张伟成',
      postName: '用户界面/交互设计师',
      grade: '中级',
    },
    {
      imgSrc: 'http://localhost:3000/tx/tx.png',
      name: '张伟成',
      postName: '用户界面/交互设计师',
      grade: '中级',
    },
    {
      imgSrc: 'http://localhost:3000/tx/tx.png',
      name: '张伟成',
      postName: '用户界面/交互设计师',
      grade: '中级',
    },
    {
      imgSrc: 'http://localhost:3000/tx/tx.png',
      name: '张伟成',
      postName: '用户界面/交互设计师',
      grade: '中级',
    },
    {
      imgSrc: 'http://localhost:3000/tx/tx.png',
      name: '张伟成',
      postName: '用户界面/交互设计师',
      grade: '中级',
    },
  ];
  let nearList = [
    {
      myColor: 'new',
      title: '新部门介绍，标题文字介绍显示 ',
      imgSrc: 'http://localhost:3000/tx/top.png',
      time: '今天| 下午5:00',
      data: '2h',
    },
    {
      myColor: 'birthday',
      title: 'Mars的生日',
      imgSrc: 'http://localhost:3000/tx/down.png',
      time: '今天|上午:10:00',
      data: '5h',
    },
    {
      myColor: 'new',
      title: '新部门介绍，标题文字介绍显示 ',
      imgSrc: 'http://localhost:3000/tx/top.png',
      time: '今天| 下午5:00',
      data: '2h',
    },
    {
      myColor: 'birthday',
      title: 'Mercury的生日',
      imgSrc: 'http://localhost:3000/tx/down.png',
      time: '今天|上午:10:00',
      data: '5h',
    },
  ];

  let projectList = [
    {
      imgSrc: 'http://localhost:3000/tx/medical.png',
      numbers: 'QK0001265',
      name: '医疗应用(iOS原生)',
      dateImg: 'http://localhost:3000/tx/date.png',
      trendImg: 'http://localhost:3000/tx/top.png',
      startTime: '创建与20209月12日',
      allTask: '34',
      emergent: '中',
      currentTask: '13',
      executor: '123',
    },
    {
      imgSrc: 'http://localhost:3000/tx/mealDelivery.png',
      numbers: 'QK0001266',
      name: '医疗应用(iOS原生)',
      dateImg: 'http://localhost:3000/tx/date.png',
      trendImg: 'http://localhost:3000/tx/top.png',
      startTime: '创建与20209月12日',
      allTask: '34',
      emergent: '中',
      currentTask: '13',
      executor: '123',
    },
    {
      imgSrc: 'http://localhost:3000/tx/Meal.png',
      numbers: 'QK0001267',
      name: '医疗应用(iOS原生)',
      dateImg: 'http://localhost:3000/tx/date.png',
      trendImg: 'http://localhost:3000/tx/down.png',
      emergent: '低',
      startTime: '创建与20209月12日',
      allTask: '34',
      currentTask: '13',
      executor: '123',
    },
  ];

  let loggerList = [
    {
      name: 'Mars',
      id: '1',
      txImg: 'http://localhost:3000/tx/tx.png',
      postName: '前端工程师',
      operate: [
        {
          qid: '11',
          srcImg: 'http://localhost:3000/tx/upload.png',
          describe: '完成仪表盘的页面搭建',
        },
        {
          qid: '12',
          srcImg: 'http://localhost:3000/tx/attach.png',
          describe: '提交PR',
        },
      ],
    },
    {
      name: 'Mercury',
      id: '2',
      txImg: 'http://localhost:3000/tx/tx.png',
      postName: 'UI设计师',
      operate: [
        {
          qid: '11',
          srcImg: 'http://localhost:3000/tx/upload.png',
          describe: 'UI组件库上传npm',
        },
      ],
    },
  ];

  return (
    <div className={styles.dashboard}>
      <div className={styles.useMessage}>欢迎回来,{user}</div>
      <div className={styles.view}>
        <div className={styles.dashboardList}>
          <div className={styles.p}>仪表盘</div>
          <div className={styles.timing}>2023年5月13日-2023年6月12日</div>
        </div>
        <div className={styles.list}>
          <div className={styles.workload}>
            <div className={styles.head}>
              <span className={styles.work}>工作量</span>
              <span>查看全部&gt;</span>
            </div>
            <div className={styles.showList}>
              {arrList.map((item, index) => {
                return (
                  <div className={styles.item} key={index}>
                    <img src={item.imgSrc} alt="" />
                    <p>{item.name}</p>
                    <p>{item.postName}</p>
                    <span>{item.grade}</span>
                  </div>
                );
              })}
            </div>
          </div>
          <div className={styles.recentEvents}>
            <div className={styles.head}>
              <span className={styles.work}>最近事件</span>
              <span
                onClick={() => {
                  history.push('/dashboard/nearList');
                }}
              >
                产看全部&gt;
              </span>
            </div>
            {nearList.map((item, index) => {
              return (
                <div className={styles.EventsList} key={index}>
                  <div className={styles.item}>
                    <div className={styles.content}>
                      <span> {item.title} </span>
                      <img src={item.imgSrc} alt="" />
                    </div>
                    <div className={styles.data}>
                      <span className={styles.date}>{item.time} </span>
                      <span> {item.data} </span>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className={styles.projectList}>
          <div className={styles.workload}>
            <div className={styles.head}>
              <span className={styles.work}>项目</span>
              <span>查看全部&gt;</span>
            </div>
            <div className={styles.showList}>
              {projectList.map((item, index) => {
                return (
                  <div className={styles.item} key={item.numbers}>
                    <div className={styles.left}>
                      <div className={styles.show}>
                        <img src={item.imgSrc} alt="" />
                        <div>
                          <span>{item.numbers}</span>
                          <p>{item.name}</p>
                        </div>
                      </div>
                      <div className={styles.startData}>
                        <div>
                          <img src="http://localhost:3000/tx/date.png" alt="" />
                          {item.startTime}
                        </div>
                        <div>
                          {' '}
                          <img src={item.trendImg} alt="" /> {item.emergent}{' '}
                        </div>
                      </div>
                    </div>
                    <div className={styles.right}>
                      <p> 项目数据 </p>
                      <div className={styles.task}>
                        <div className={styles.item}>
                          <span> 所有任务 </span>
                          <div>{item.allTask}</div>
                        </div>
                        <div className={styles.item}>
                          <span> 当前任务 </span>
                          <div>{item.currentTask}</div>
                        </div>
                        <div className={styles.item}>
                          <span> 执行人 </span>
                          <div>{item.executor}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className={styles.recentEvents}>
            <div className={styles.head}>
              <span className={styles.work}>项目日志</span>
            </div>
            {loggerList.map((item) => {
              return (
                <div className={styles.message} key={item.id}>
                  <div className={styles.user}>
                    <img src={item.txImg} alt="" />
                    <div>
                      <p>{item.name}</p>
                      <span>{item.postName}</span>
                    </div>
                  </div>
                  {item.operate.map((ele, index) => {
                    return (
                      <div className={styles.itemList} key={ele.qid}>
                        <img src={ele.srcImg} alt="" />
                        <p>{ele.describe}</p>
                      </div>
                    );
                  })}
                </div>
              );
            })}
            <div className={styles.tip}>查看更多</div>
          </div>
        </div>
      </div>
    </div>
  );
}
