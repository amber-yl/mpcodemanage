import React, { useState } from 'react';
import { getMonthDays, numberToString } from '../../utils/getMonthDays';
import { Modal, Button, Radio, Calendar } from 'antd';
import type { RadioChangeEvent } from 'antd';
import type { CalendarMode } from 'antd/es/calendar/generateCalendar';
import type { Dayjs } from 'dayjs';
import locale from 'antd/lib/calendar/locale/zh_CN.js';
import style from './index.module.less';
const onPanelChange = (value: Dayjs, mode: CalendarMode) => {
  console.log(value.format('YYYY-MM-DD'), mode);
};
export default function vacation() {
  let [active, setActive] = useState(0);
  let [alertActive, setAlertActive] = useState(0);
  let [length, setLength] = useState(getMonthDays(Date.now()).days);
  let [mounth, setMounth] = useState(getMonthDays(Date.now()).mounth);
  const [modal2Open, setModal2Open] = useState(false);
  let mountString = numberToString(mounth);
  function setNewMounth(str: string) {
    if (str == '+') {
      if (mounth >= 12) {
        setMounth(1);
      } else {
        setMounth((mounth += 1));
      }
      setLength(getMonthDays(Date.now(), mounth).days);
    } else {
      if (mounth <= 1) {
        setMounth(12);
      } else {
        setMounth((mounth -= 1));
      }
      setLength(getMonthDays(Date.now(), mounth).days);
    }
  }
  const [value, setValue] = useState(1);

  const onChange = (e: RadioChangeEvent) => {
    console.log('radio checked', e.target.value);
    setValue(e.target.value);
  };
  return (
    <div className={style.pages}>
      <Modal
        title="我要请假"
        centered
        open={modal2Open}
        onCancel={() => setModal2Open(false)}
        footer={[
          <Button
            type="primary"
            key="back"
            onClick={() => setModal2Open(false)}
          >
            提交请求
          </Button>,
        ]}
      >
        <div>
          <Radio.Group onChange={onChange} value={value}>
            <Radio value={1}>调休</Radio>
            <Radio value={2}>病假</Radio>
            <Radio value={3}>远程办公</Radio>
          </Radio.Group>
        </div>
        <div id="alert-center">
          <div className="center">
            <div>
              <p
                className={alertActive == 0 ? 'active' : ''}
                onClick={() => {
                  setAlertActive(0);
                }}
              >
                天
              </p>
              <p
                className={alertActive == 1 ? 'active' : ''}
                onClick={() => {
                  setAlertActive(1);
                }}
              >
                小时
              </p>
            </div>
            <div
              className={alertActive == 0 ? 'sliding' : 'sliding sliding-right'}
            ></div>
          </div>
        </div>
        <div>
          <Calendar locale={locale} fullscreen={false} />
        </div>
      </Modal>
      <div className="header">
        <h2>假期</h2>
        <div className="center">
          <div>
            <p
              className={active == 0 ? 'active' : ''}
              onClick={() => {
                setActive(0);
              }}
            >
              员工假期
            </p>
            <p
              className={active == 1 ? 'active' : ''}
              onClick={() => {
                setActive(1);
              }}
            >
              日历
            </p>
          </div>
          <div
            className={active == 0 ? 'sliding' : 'sliding sliding-right'}
          ></div>
        </div>
        <button onClick={() => setModal2Open(true)}>+添加请求</button>
      </div>
      {active == 0 && (
        <ul className="list_main">
          <li>
            <div className="left">
              <img src="http://localhost:3000/users/users(1).png" alt="" />
              <div>
                <p>金金小张</p>
                <span>jinjinuidesign@hotmail.com</span>
              </div>
            </div>
            <div className="right">
              <div className="r-item">
                <p>调休</p>
                <span>15</span>
              </div>
              <div className="r-item">
                <p>病假</p>
                <span>3</span>
              </div>
              <div className="r-item">
                <p>远程办公</p>
                <span>50</span>
              </div>
            </div>
          </li>
          <li>
            <div className="left">
              <img src="http://localhost:3000/users/users(1).png" alt="" />
              <div>
                <p>金金小张</p>
                <span>jinjinuidesign@hotmail.com</span>
              </div>
            </div>
            <div className="right">
              <div className="r-item">
                <p>调休</p>
                <span>15</span>
              </div>
              <div className="r-item">
                <p>病假</p>
                <span>3</span>
              </div>
              <div className="r-item">
                <p>远程办公</p>
                <span>50</span>
              </div>
            </div>
          </li>
          <li>
            <div className="left">
              <img src="http://localhost:3000/users/users(1).png" alt="" />
              <div>
                <p>金金小张</p>
                <span>jinjinuidesign@hotmail.com</span>
              </div>
            </div>
            <div className="right">
              <div className="r-item">
                <p>调休</p>
                <span>15</span>
              </div>
              <div className="r-item">
                <p>病假</p>
                <span>3</span>
              </div>
              <div className="r-item">
                <p>远程办公</p>
                <span>50</span>
              </div>
            </div>
          </li>
          <li>
            <div className="left">
              <img src="http://localhost:3000/users/users(1).png" alt="" />
              <div>
                <p>金金小张</p>
                <span>jinjinuidesign@hotmail.com</span>
              </div>
            </div>
            <div className="right">
              <div className="r-item">
                <p>调休</p>
                <span>15</span>
              </div>
              <div className="r-item">
                <p>病假</p>
                <span>3</span>
              </div>
              <div className="r-item">
                <p>远程办公</p>
                <span>50</span>
              </div>
            </div>
          </li>
          <li>
            <div className="left">
              <img src="http://localhost:3000/users/users(1).png" alt="" />
              <div>
                <p>金金小张</p>
                <span>jinjinuidesign@hotmail.com</span>
              </div>
            </div>
            <div className="right">
              <div className="r-item">
                <p>调休</p>
                <span>15</span>
              </div>
              <div className="r-item">
                <p>病假</p>
                <span>3</span>
              </div>
              <div className="r-item">
                <p>远程办公</p>
                <span>50</span>
              </div>
            </div>
          </li>
        </ul>
      )}
      {active == 0 && (
        <div className="paging">
          <span>1</span>
          <span>-</span>
          <span>8</span>
          <span>of</span>
          <span>28</span>
          <span className="previous">←</span>
          <span className="next">→</span>
        </div>
      )}
      {active == 1 && (
        <div className="calendarBox">
          <div className="table_header">
            <div className="left">
              <span>员工</span>
              <p>🔍</p>
            </div>
            <div className="right">
              <div className="top">
                <p></p>
                <p>第一个月({mountString}月)</p>
                <p>
                  <span
                    className="zuo"
                    onClick={() => {
                      setNewMounth('-');
                    }}
                  >
                    ←
                  </span>
                  <span
                    className="you"
                    onClick={() => {
                      setNewMounth('+');
                    }}
                  >
                    →
                  </span>
                </p>
              </div>
              <div className="bottom">
                {Array.from({ length }, (_, index) => {
                  return index + 1;
                }).map((day: any, index: number) => {
                  return (
                    <div className="day" key={index}>
                      {day}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <div className="table_main">
            <div className="left">
              <ul>
                <li>
                  <img src="http://localhost:3000/users/users(1).png" alt="" />
                  <span>金金小张</span>
                </li>
                <li>
                  <img src="http://localhost:3000/users/users(1).png" alt="" />
                  <span>金金小张</span>
                </li>
                <li>
                  <img src="http://localhost:3000/users/users(1).png" alt="" />
                  <span>金金小张</span>
                </li>
                <li>
                  <img src="http://localhost:3000/users/users(1).png" alt="" />
                  <span>金金小张</span>
                </li>
                <li>
                  <img src="http://localhost:3000/users/users(1).png" alt="" />
                  <span>金金小张</span>
                </li>
              </ul>
            </div>
            <div className="right">
              <ul>
                <li>
                  {Array.from({ length }, (_, index) => {
                    return index + 1;
                  }).map((day: any, index: number) => {
                    return <div className="day" key={index}></div>;
                  })}
                </li>
                <li>
                  {Array.from({ length }, (_, index) => {
                    return index + 1;
                  }).map((day: any, index: number) => {
                    return <div className="day" key={index}></div>;
                  })}
                </li>
                <li>
                  {Array.from({ length }, (_, index) => {
                    return index + 1;
                  }).map((day: any, index: number) => {
                    return <div className="day" key={index}></div>;
                  })}
                </li>
                <li>
                  {Array.from({ length }, (_, index) => {
                    return index + 1;
                  }).map((day: any, index: number) => {
                    return <div className="day" key={index}></div>;
                  })}
                </li>
                <li>
                  {Array.from({ length }, (_, index) => {
                    return index + 1;
                  }).map((day: any, index: number) => {
                    return <div className="day" key={index}></div>;
                  })}
                </li>
              </ul>
            </div>
          </div>
          <div className="table_footer">
            <div className="left"></div>
            <div className="right">
              <div className="item">
                <p>病假</p>
                <div className="bottom">
                  <div className="audit">
                    <div></div>
                    <span>已批准</span>
                  </div>
                  <div className="audited">
                    <div></div>
                    <span>审核中</span>
                  </div>
                </div>
              </div>
              <div className="item">
                <p>远程办公</p>
                <div className="bottom  remote-control">
                  <div className="audit">
                    <div></div>
                    <span>已批准</span>
                  </div>
                  <div className="audited">
                    <div></div>
                    <span>审核中</span>
                  </div>
                </div>
              </div>
              <div className="item">
                <p>调休</p>
                <div className="bottom change-rest">
                  <div className="audit">
                    <div></div>
                    <span>已批准</span>
                  </div>
                  <div className="audited">
                    <div></div>
                    <span>审核中</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
