import { Redirect } from 'umi';

export default function Auth(props: any) {
  //获取本地缓存 localStorage的值
  const isLogin = localStorage.getItem('token');
  console.log(isLogin, 'isLogin');

  //如果这个值存在就返回正常内容
  if (isLogin) {
    return <div>{props.children}</div>;
  } else {
    //如果这个值存在就重定向到登录页
    return <Redirect to="/login" />;
  }
}
