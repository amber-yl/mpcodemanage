//获取当月天数
export function getMonthDays(date: any, mounth?: number) {
  var date: any = new Date(date);
  // 方法2
  if (!mounth) {
    date.setMonth(date.getMonth() + 1);
  } else {
    date.setMonth(mounth);
  }
  date.setDate(0);
  return { days: date.getDate(), mounth: date.getMonth() + 1 };
}
//1-12转汉字大写
export function numberToString(num: Number) {
  if (num == 1) {
    return '一';
  } else if (num == 2) {
    return '二';
  } else if (num == 3) {
    return '三';
  } else if (num == 4) {
    return '四';
  } else if (num == 5) {
    return '五';
  } else if (num == 6) {
    return '六';
  } else if (num == 7) {
    return '七';
  } else if (num == 8) {
    return '八';
  } else if (num == 9) {
    return '九';
  } else if (num == 10) {
    return '十';
  } else if (num == 11) {
    return '十一';
  } else if (num == 12) {
    return '十二';
  }
}
