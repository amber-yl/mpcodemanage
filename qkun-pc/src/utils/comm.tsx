/**
 * 获取token
 * @returns
 */
export const getToken = () => {
  let token = localStorage.getItem('token');
  return token;
};

/**
 * 设置token，讲token存到本地当中
 * @param token
 */
export const setToken = (token: any) => {
  localStorage.setItem('token', token);
};
