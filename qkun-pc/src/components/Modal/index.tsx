import React, { ReactNode ,Ref,useRef} from 'react';
import { Modal ,Input } from 'antd';
import { PlusOutlined } from '@ant-design/icons'
import "./index.less"
const MyModal: React.FC = (props:any) => {
  const contentInput:React.ForwardedRef<any> = useRef()
  let {title,open,confirmLoading,handleOk,handleCancel,cancelText,okText} = props
  //创建Input框
  const createInput = () => {
    return <Input placeholder="请填写员工邮箱" ref={contentInput} />;
  };
  //继续添加
  const aginInput = (value) => {
    console.log(value);
  };
  return (
    <>
      <Modal
        title={title}
        open={open}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        cancelText={cancelText}
        okText={okText}
        modalRender={(modal: ReactNode) => <div>{modal}</div>}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <img
            className="img"
            src="http://localhost:3000/employeeImages/illustration.png"
            alt=""
          />
        </div>
        <div className="ModalContent">
          <p>员工邮箱</p>
          {createInput()}
          <div className='btn'>
            <PlusOutlined/>
            <button className='button' onClick={()=>aginInput(contentInput.current.input.value)}>继续添加</button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default MyModal;
