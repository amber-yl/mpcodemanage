import React from 'react';
import styles from './index.less';

export default function Head() {
  return (
    <div className={styles.head}>
      <input type="text" placeholder="搜索" />
      <div className="message">
        <div className="messList"> 消息 </div>
        <div className="user"> 张三 </div>
      </div>
    </div>
  );
}
