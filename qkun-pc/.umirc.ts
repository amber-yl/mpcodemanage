import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    {
      path: '/login',
      component: '@/pages/login/index',
      meta: {},
      exact: true,
    },
    
    {
      path: '/register',
      component: '@/pages/register/index',
      meta: {},
      exact: true,
    },
    {
      path: '/',
      component: '@/layouts/index',
      meta: {},
      routes: [
        {
          path: '/dashboard',
          component: '@/pages/dashboard/index',
          meta: { title: '仪表盘', icon: '', isRoute:true},
          exact: true,
          // wrappers: ['@/wrappers/Auth'],
        },
        {
          path: '/project',
          component: '@/pages/project/index',
          meta: { title: '项目', icon: '' , isRoute:true},
          exact: true,
          // wrappers: ['@/wrappers/Auth'],
        },
        {
          path: '/calendar',
          component: '@/pages/calendar/index',
          meta: { title: '日历', icon: '', isRoute:true },
          exact: true,
          // wrappers: ['@/wrappers/Auth'],
        },
        {
          path: '/vacation',
          component: '@/pages/vacation/index',
          meta: { title: '假期', icon: '', isRoute:true },
          exact: true,
          // wrappers: ['@/wrappers/Auth'],
        },
        {
          path: '/employee',
          component: '@/pages/employee/index',
          meta: { title: '雇员', icon: '', isRoute:true },
          exact: true,
          // wrappers: ['@/wrappers/Auth'],
        },
        {
          path: '/message',
          component: '@/pages/message/index',
          meta: { title: '消息', icon: '' , isRoute:true },
          exact: true,
          // wrappers: ['@/wrappers/Auth'],
        },
        {
          path: '/website',
          component: '@/pages/website/index',
          meta: { title: '门户网站', icon: '' , isRoute:true  },
          exact: true,
          // wrappers: ['@/wrappers/Auth'],
        },
        {
          path:'/dashboard/nearList',
          component: '@/pages/dashboard/nearList/index',
          meta: { isRoute:false },
        },
        {
          path: '/website/medical',
          component: '@/pages/website/medical/index',
          meta: { isRoute:false },
        }
      ],
    },
  ],
  fastRefresh: {},
  // antd:{}
});
