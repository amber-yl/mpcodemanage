import { Injectable } from '@nestjs/common';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Auth } from './schemas/auth.schema';
import { Model } from 'mongoose';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(Auth.name)
    private readonly authModel: Model<Auth>,
    private jwtService: JwtService,
  ) {}

  // create(createAuthDto: CreateAuthDto) {
  //   return 'This action adds a new auth';
  // }

  // findAll() {
  //   return `This action returns all auth`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} auth`;
  // }

  // update(id: number, updateAuthDto: UpdateAuthDto) {
  //   return `This action updates a #${id} auth`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} auth`;
  // }

  async login(body: any) {
    const { email, password } = body;

    let data = await this.authModel
      .find({
        email,
        password,
      })
      .exec();
    if (data.length === 0) {
      return { code: 0, msg: '账号密码错误！' };
    } else {
      let payload = {
        id: data[0].id,
      };
      return {
        code: 1,
        msg: '登录成功',
        data: this.jwtService.sign(payload),
      };
    }
  }

  async create(body): Promise<any> {
    const createdAuth = await this.authModel.create(body);
    return createdAuth;
  }

  async getTelCode(body): Promise<any> {
    const { tel } = body;

    const res = await this.authModel
      .find({
        tel: tel,
      })
      .exec();
    if (res.length === 0) {
      // 发送验证码
      let number = Math.floor(Math.random() * (10000 - 1000) + 1000);
      return {
        code: 1,
        msg: '验证码发送成功，请注意查收！',
        data: {
          tel,
          code: '' + number,
          time: new Date().getTime(),
        },
      };
    } else {
      // 当前手机号已经被注册过
      return { code: 0, msg: '当前手机号已存在，不能重复注册！' };
    }
  }
}
