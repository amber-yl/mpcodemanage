// @Prop 装饰器接受一个可选的参数，通过这个，你可以指示这个属性是否是必须的，是否需要默认值，或者是标记它作为一个常量，下面是例子
// SchemaFactory 是 mongoose 内置的一个方法做用是读取模式文档 并创建 Schema 对象
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type CatDocument = HydratedDocument<Auth>;

@Schema()
export class Auth {
  @Prop()
  // 姓名
  username: string;

  @Prop()
  // 密码
  password: string;

  @Prop()
  // 头像
  photo: string;

  // 设置值为必填
  @Prop({ required: true })
  // 邮箱
  email: string;

  @Prop()
  // 手机号码
  tel: string;

  @Prop()
  // 职位
  jobTitle: string;

  @Prop()
  // 公司
  corporation: string;

  @Prop()
  // 地点
  place: string;

  @Prop()
  // 出生日期
  birthday: string;

  @Prop()
  // 钉钉号码
  dingTalk: string;
}

export const AuthSchema = SchemaFactory.createForClass(Auth);
