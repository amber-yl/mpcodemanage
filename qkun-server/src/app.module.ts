import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ProjectModule } from './project/project.module';
import { CalendarModule } from './calendar/calendar.module';
import { VacationModule } from './vacation/vacation.module';
import { EmployeeModule } from './employee/employee.module';
import { MessageModule } from './message/message.module';
import { WebsiteModule } from './website/website.module';
import { RegisterModule } from './register/register.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://127.0.0.1/test'),
    AuthModule,
    DashboardModule,
    ProjectModule,
    CalendarModule,
    VacationModule,
    EmployeeModule,
    MessageModule,
    WebsiteModule,
    RegisterModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
