import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { RegisterService } from './register.service';
import { CreateRegisterDto } from './dto/create-register.dto';
import { UpdateRegisterDto } from './dto/update-register.dto';
import { AuthService } from 'src/auth/auth.service';

@Controller('register')
export class RegisterController {
  constructor(
    private readonly registerService: RegisterService,
    private readonly authService: AuthService,
  ) {}

  // @Post()
  // create(@Body() createRegisterDto: CreateRegisterDto) {
  //   return this.registerService.create(createRegisterDto);
  // }

  // @Get()
  // findAll() {
  //   return this.registerService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.registerService.findOne(+id);
  // }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateRegisterDto: UpdateRegisterDto,
  // ) {
  //   return this.registerService.update(+id, updateRegisterDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.registerService.remove(+id);
  // }

  @Post('getTelCode')
  async getTelCode(@Body() body) {
    let res = await this.authService.getTelCode(body);
    if (res.code === 1) {
      await this.registerService.create(res.data);
    }
    return res;
  }
}
