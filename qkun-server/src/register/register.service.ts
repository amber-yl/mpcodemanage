import { Injectable } from '@nestjs/common';
import { CreateRegisterDto } from './dto/create-register.dto';
import { UpdateRegisterDto } from './dto/update-register.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Register } from './schemas/register.schema';
import { Model } from 'mongoose';

@Injectable()
export class RegisterService {
  constructor(
    @InjectModel(Register.name)
    private readonly registerModel: Model<Register>,
  ) {}

  // create(createRegisterDto: CreateRegisterDto) {
  //   return 'This action adds a new register';
  // }

  // findAll() {
  //   return `This action returns all register`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} register`;
  // }

  // update(id: number, updateRegisterDto: UpdateRegisterDto) {
  //   return `This action updates a #${id} register`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} register`;
  // }

  async create(body): Promise<any> {
    await this.registerModel.create(body);
  }
}
