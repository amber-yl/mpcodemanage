// @Prop 装饰器接受一个可选的参数，通过这个，你可以指示这个属性是否是必须的，是否需要默认值，或者是标记它作为一个常量，下面是例子
// SchemaFactory 是 mongoose 内置的一个方法做用是读取模式文档 并创建 Schema 对象
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type CatDocument = HydratedDocument<Register>;

@Schema()
export class Register {
  @Prop()
  // 手机号
  tel: string;

  @Prop()
  // 验证码
  code: string;

  @Prop()
  // 时间
  time: number;
}

export const registerSchema = SchemaFactory.createForClass(Register);
