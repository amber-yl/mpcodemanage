import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Employee } from './schema/employee.schema';
import { Model } from 'mongoose';
@Injectable()
export class EmployeeService {
  constructor(
    @InjectModel(Employee.name)
    private readonly employeeModel: Model<Employee>,
  ) {}
  async create(body: any): Promise<any> {
    const createEmployee = await this.employeeModel.create(body);
    return createEmployee;
  }

  async findAll(): Promise<Employee[]> {
    return this.employeeModel.find().exec();
  }
  async findOne(id: string): Promise<Employee> {
    return this.employeeModel.findOne({ _id: id }).exec();
  }
  async delete(id: string) {
    const deletedCat = await this.employeeModel
      .findByIdAndRemove({ _id: id })
      .exec();
    return deletedCat;
  }
  // findOne(id: number) {
  //   return `This action returns a #${id} employee`;
  // }

  // update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
  //   return `This action updates a #${id} employee`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} employee`;
  // }
}
