import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type EmployeeDocument = Employee & Document;

@Schema()
export class Employee extends Document {
  @Prop()
  typeName: string;
  @Prop()
  sex: string;
  @Prop()
  age: number;
  @Prop()
  typeEmail: string;
  @Prop()
  typeDate: string;
  @Prop()
  typePosition: string;
  @Prop()
  typeGrade: string;
  @Prop()
  beginNum: number;
  @Prop()
  conductNum: number;
  @Prop()
  checkNum: number;
}

export const EmployeeSchema = SchemaFactory.createForClass(Employee);
