import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as path from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(path.join(process.cwd(), './public'));
  app.enableCors();
  // swagger 配置
  const config = new DocumentBuilder()
    .setTitle('乾坤项目管理api文档') // api文档标题
    .setDescription('dashboard(仪表盘)api,project(项目)api,calendar(日历)api,') // api文档描述
    .setVersion('1.0') // api文档版本
    // .addTag('') // 标签
    .addBearerAuth() // token鉴权
    .build();
  const document = SwaggerModule.createDocument(app, config);
  // setup第一个参数为访问路径（如：http://localhost:3000/api），一般不用api，替换成api-docs,防止跟接口冲突
  // SwaggerModule.setup('api', app, document);
  SwaggerModule.setup('api-docs', app, document);
  await app.listen(3000);
}
bootstrap();
