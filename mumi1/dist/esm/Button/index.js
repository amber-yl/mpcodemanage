import React from 'react';
export default function button(_ref) {
  var onClick = _ref.onClick,
    children = _ref.children;
  return /*#__PURE__*/React.createElement("button", {
    onClick: onClick
  }, children);
}