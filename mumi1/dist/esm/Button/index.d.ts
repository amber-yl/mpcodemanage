import React, { ReactNode } from 'react';
export interface Button {
    /**
     * @description 这是个按钮内容
     *
     * @default '这是一个按钮'
     *
     */
    children?: ReactNode;
    /**
     * @description 选择类型
     *
     * @default  undefined
     *
     */
    type?: 'link' | 'text' | 'ghost' | 'default' | 'primary' | 'dashed' | undefined;
    /**
     * @description 这是个按钮的点击事件
     *
     * @default  (event:any) => void
     *
     */
    onClick?: (event: any) => void;
}
export default function button({ onClick, children }: Button): React.JSX.Element;
