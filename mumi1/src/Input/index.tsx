import React from 'react';
import "./index.less"

interface Input {
  /**
   * @description 元素的text值
   *
   * @default "text"
   *
   */
  type?: "text";
}

export default function Input({type}:Input) {
  return <input type={type} className={'input'} />;
}
