import React, { ReactNode, useEffect, useRef } from 'react';
import './index.less';

interface Button {
  /**
   * @description 按钮内容
   *
   * @default
   *
   */
  children?: ReactNode;
  /**
   * @description 选择按钮类型
   *
   * @default  "primary"
   *
   */
  type?: 'primary' | 'success' | 'danger' | 'warning';
  /**
   * @description 按钮的点击事件
   *
   * @default  (event:any) => void
   *
   */
  onClick?: (event: any) => void;
  /**
   * @description 按钮的大小
   *
   * @default  "normal"
   *
   */
  size?: 'big' | 'normal' | 'small';
  /**
   * @description 是否禁用
   *
   * @default  false
   *
   */
  disabled?: false | true;
  /**
   * @description 是否为块级元素
   *
   * @default  false
   *
   */
  block?: false | true;
  /**
   * @description 是否为圆角按钮
   *
   * @default  false
   *
   */
  radius?: false | true;
  /**
   * @description 自定义样式，可轻松覆盖其原始样式，需要将自己的样式引入放入mumi样式后; 您的样式优先级最高
   *
   * @default
   *
   */
  className?: Object;
  /**
   * @description 获取元素的ref
   *
   * @default 
   *
   */
  refs?: any;
}

export default function button({
  onClick,
  type,
  size,
  disabled,
  block,
  className,
  children,
  radius,
  refs
}: Button) {
  return (
    <button
      ref={refs}
      disabled={disabled}
      className={`button ${type ? type : ''} ${size ? size : ''} ${block ? 'block' : ''}  ${
        className ? className : ''
      } ${disabled ? 'disabled' : ''} ${radius ? 'radius' : ''}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
