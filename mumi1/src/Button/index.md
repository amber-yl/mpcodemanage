## Button
```tsx
import React,{useState ,useRef,useEffect } from 'react';
import { Button } from 'mumi1';

export default () =>  {
    return (
        <Button radius={true} onClick={()=>{  alert("这是一个按钮") }}>这是按钮</Button>
    )
};
```
<API></API>

查看更多示例: https://d.umijs.org/guide/basic#write-component-demo
